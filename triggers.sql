select * from orders

select * from serviceList
update serviceList set hours = 10 where id = 1
create TRIGGER totalHoursCount  
ON serviceList
AFTER INSERT, UPDATE   
AS 
If ((SELECT count(*) FROM INSERTED where status = 100) > 0)
	Begin
		rollback tran
		Return
	End
declare  @orderId int
--
DECLARE cursorForInserted CURSOR FOR   
SELECT distinct idOrder FROM inserted

OPEN cursorForInserted   

FETCH NEXT FROM cursorForInserted   
INTO @orderId

WHILE @@FETCH_STATUS = 0  
BEGIN  


declare @totalHours int, @totalPrice int, @serviceId int, @hours int, @pricePerHour int

set @totalPrice = 0
select @orderId = (select top 1  inserted.idOrder from inserted)

select @totalHours = sum(hours)  from serviceList 
where idOrder = @orderId 
---
DECLARE cursorForTotalPrice CURSOR FOR   
SELECT  idService, hours
FROM serviceList
WHERE idOrder = @orderId 

OPEN cursorForTotalPrice   

FETCH NEXT FROM cursorForTotalPrice   
INTO @serviceId, @hours

WHILE @@FETCH_STATUS = 0  
BEGIN  
	select @pricePerHour = pricePerHour from services where id = @serviceId
	set @totalPrice = @totalPrice + @pricePerHour * @hours
	FETCH NEXT FROM cursorForTotalPrice   
	INTO @serviceId, @hours
END   
CLOSE cursorForTotalPrice ;  
DEALLOCATE cursorForTotalPrice ;  
---
--select @totalHours as totalHours
--select @orderId as orderId
--select @totalPrice as totalPrice

update orders set hoursTotal = @totalHours, priceTotal = @totalPrice where id = @orderId
FETCH NEXT FROM cursorForInserted   
	INTO @orderId
END   
CLOSE cursorForInserted ;  
DEALLOCATE cursorForInserted ;  

GO  

update orders set finishDate = null
select * from orders
select * from serviceList
update serviceList set hours = 20  where id =2
update serviceList set hours = hours+1-1
alter TRIGGER finishDateTrigger
ON serviceList
AFTER INSERT, UPDATE   
AS 
declare @orderId int, @totalHours int
--
DECLARE cursorForInserted CURSOR FOR   
SELECT distinct idOrder FROM inserted

OPEN cursorForInserted   

FETCH NEXT FROM cursorForInserted   
INTO @orderId

WHILE @@FETCH_STATUS = 0  
BEGIN  
	select @totalHours = sum(hours) from serviceList where idOrder = @orderId	
	--select ceiling(1.0*@totalHours/8) as daysToAdd
	--select floor(1.0*@totalHours/8/5)*2 as weekendsToAdd
	--select ceiling(1.0*@totalHours/8)+floor(1.0*@totalHours/8/5)*2 as totalDaysToAdd
	update orders set finishDate = dateadd(DAY, ceiling(1.0*@totalHours/8)+floor(1.0*@totalHours/8/5)*2, startDate) where id = @orderId

	FETCH NEXT FROM cursorForInserted   
	INTO @orderId
END   
CLOSE cursorForInserted ;  
DEALLOCATE cursorForInserted ;  

go

select * from orders where idTeam = 9

select * from Users

select * from orders
--proc 1
exec MostOrderedServices '2018-11-01','2018-12-20' 

alter PROCEDURE MostOrderedServices @from date, @to date
AS
select services.name ,
sum(hours) as totalHoursByService,
(select sum(hours) from serviceList 
inner join orders on orders.id = idOrder
where orders.startDate between @from and @to) as totalHours 
from serviceList 
inner join services on services.id = idService
inner join orders on orders.id = idOrder
where orders.startDate between @from and @to
group by services.name
go
--proc 2
exec OrdersPriceToHoursProcedure

alter procedure OrdersPriceToHoursProcedure
as
select id,hoursTotal,priceTotal from orders
where hoursTotal > 0 and priceTotal > 0
order by hoursTotal asc
go

-- proc 3
exec ordersAmountPerYear 2018

create proc ordersAmountPerYear @year int
as
select count(*) as ordersAmount,MONTH(startDate) as month from orders
where year(startDate) = @year
group by MONTH(startDate)
go

--proc for manager by 1 team and by year
exec hoursWorkedAtTeam 2018, 1

create proc hoursWorkedAtTeam @year int, @teamId int
as
select sum(serviceList.hours) as hoursWorked, staff.name from serviceList
inner join staff on staff.id = idEmployee
inner join orders on orders.id = idOrder
inner join team on team.id = staff.idTeam
where year(orders.startDate) = @year and team.id = @teamId
group by staff.name
go

--proc for admin by all teams and by year
exec hoursWorkedByAllTeams 2018

create proc hoursWorkedByAllTeams @year int
as
select sum(serviceList.hours) as hoursWorked, team.name from serviceList
inner join staff on staff.id = idEmployee
inner join orders on orders.id = idOrder
inner join team on team.id = staff.idTeam
where year(orders.startDate) = @year 
group by team.name
go