﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using CourseWork.Attributes;
using CourseWork.Models;
using Newtonsoft.Json;

namespace CourseWork.Controllers
{
    [MyAuthorize(Roles = "admin, projManager, Employee, client")]
    public class serviceListsController : Controller
    {
        private designStudioEntities db = new designStudioEntities();
        private User currentUser;

        private User GetCurrentUser()
        {
            IEnumerable<Claim> claims = ((ClaimsIdentity)User.Identity).Claims;
            var id = int.Parse(claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier).Value);
            currentUser = db.Users.FirstOrDefault(u => u.id == id);
            return currentUser;
        }
        // GET: serviceLists

        public ActionResult Index(string sortOrder,string SearchString_order, string SearchString_service, string SearchString_worker, int? id)
        {
            var serviceLists = new List<serviceList>();
            GetCurrentUser();
            if (User.IsInRole("client"))
            {
                serviceLists = db.serviceLists.Where(s => s.order.id == id).Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();

                return View(serviceLists);
            }
            serviceLists = db.serviceLists.Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam;
                serviceLists = db.serviceLists.Where(s => s.order.idTeam == teamId).Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();
            }
            if (User.IsInRole("Employee"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam;
                serviceLists = db.serviceLists.Where(s => s.order.idTeam == teamId && s.staff.idUser == currentUser.id).Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();
            }

            ViewBag.HoursSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.StatusSortParm = sortOrder == "Status" ? "Status_desc" : "Status";
            ViewBag.OrderIdSortParm = sortOrder == "OrderId" ? "OrderId_desc" : "OrderId";
            ViewBag.StaffFioSortParm = sortOrder == "StaffFio" ? "StaffFio_desc" : "StaffFio";
            ViewBag.WorkTypeSortParm = sortOrder == "WorkType" ? "WorkType_desc" : "WorkType";

            var serviceLists_ = from sl in db.serviceLists
                          select sl;



            switch (sortOrder)
            {
                case "name_desc":
                    serviceLists_ = serviceLists_.OrderByDescending(sl => sl.hours);
                    break;

                case "Status":
                    serviceLists_ = serviceLists_.OrderBy(sl => sl.status);
                    break;
                case "Status_desc":
                    serviceLists_ = serviceLists_.OrderByDescending(sl => sl.status);
                    break;
                case "OrderId":
                    serviceLists_ = serviceLists_.OrderBy(sl => sl.idOrder);
                    break;
                case "OrderId_desc":
                    serviceLists_ = serviceLists_.OrderByDescending(sl => sl.idOrder);
                    break;
                case "StaffFio":
                    serviceLists_ = serviceLists_.OrderBy(sl => sl.staff.name);
                    break;
                case "StaffFio_desc":
                    serviceLists_ = serviceLists_.OrderByDescending(sl => sl.staff.name);
                    break;
                case "WorkType":
                    serviceLists_ = serviceLists_.OrderBy(sl => sl.service.name);
                    break;
                case "WorkType_desc":
                    serviceLists_ = serviceLists_.OrderByDescending(sl => sl.service.name);
                    break;

                    


                default:
                    serviceLists_ = serviceLists_.OrderBy(sl => sl.hours);
                    break;
            }

          


            if (!String.IsNullOrEmpty(SearchString_order) || !String.IsNullOrEmpty(SearchString_service) || !String.IsNullOrEmpty(SearchString_worker))
            {
                if (!String.IsNullOrEmpty(SearchString_order))
                {
                    int order = Convert.ToInt32(SearchString_order);
                    serviceLists_ = serviceLists_.Where(sl => sl.idOrder == order && sl.service.name.Contains(SearchString_service) && sl.staff.name.Contains(SearchString_worker)
                                       );
                }

                serviceLists_ = serviceLists_.Where(sl => sl.service.name.Contains(SearchString_service) && sl.staff.name.Contains(SearchString_worker)
                                       );
            }

            string preserveReferenacesObjects = JsonConvert.SerializeObject(serviceLists_, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            ViewBag.data = preserveReferenacesObjects;

            return View(serviceLists_);
        }

        // GET: serviceLists/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceList serviceList = db.serviceLists.Find(id);
            if (serviceList == null)
            {
                return HttpNotFound();
            }
            return View(serviceList);
        }

        // GET: serviceLists/Create

        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create()
        {
            ViewBag.idOrder = new SelectList(db.orders, "id", "id");
            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", "team.name", 1);
            ViewBag.idService = new SelectList(db.services, "id", "name");

            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }

            return View();
        }

        // POST: serviceLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create([Bind(Include = "id,idService,idOrder,idEmployee,hours,status")] serviceList serviceList)
        {
            if (ModelState.IsValid)
            {
                db.serviceLists.Add(serviceList);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idOrder = new SelectList(db.orders, "id", "id", serviceList.idOrder);
            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", serviceList.idEmployee);
            ViewBag.idService = new SelectList(db.services, "id", "name", serviceList.idService);
            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }
            return View(serviceList);
        }

        // GET: serviceLists/Edit/5
        [MyAuthorize(Roles = "admin, projManager, Employee")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceList serviceList = db.serviceLists.Find(id);
            if (serviceList == null)
            {
                return HttpNotFound();
            }
            ViewBag.idOrder = new SelectList(db.orders, "id", "id", serviceList.idOrder);

            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", "team.name", serviceList.idEmployee);

            ViewBag.idService = new SelectList(db.services, "id", "name", serviceList.idService);
            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }
            return View(serviceList);
        }

        // POST: serviceLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager, Employee")]
        public ActionResult Edit([Bind(Include = "id,idService,idOrder,idEmployee,hours,status")] serviceList serviceList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idOrder = new SelectList(db.orders, "id", "id", serviceList.idOrder);
            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", serviceList.idEmployee);
            ViewBag.idService = new SelectList(db.services, "id", "name", serviceList.idService);
            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }
            return View(serviceList);
        }

        // GET: serviceLists/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceList serviceList = db.serviceLists.Find(id);
            if (serviceList == null)
            {
                return HttpNotFound();
            }
            return View(serviceList);
        }

        // POST: serviceLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [MyAuthorize(Roles = "admin, projManager")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            serviceList serviceList = db.serviceLists.Find(id);
            db.serviceLists.Remove(serviceList);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public FileResult Export(string data)
        {


            designStudioEntities db = new designStudioEntities();

            DataTable dt = new DataTable("serviceLists");
            dt.Columns.AddRange(new DataColumn[5] { new DataColumn("Время выполнения"),
                                            new DataColumn("Статус выполнения"),
                                            new DataColumn("Id заказа"),
                                            new DataColumn("ФИО работника"),
                                            new DataColumn("Тип работы ")
                                           });





            var serviceLists = JsonConvert.DeserializeObject<List<serviceList>>(data);

            foreach (var serviceList in serviceLists)
            {
                dt.Rows.Add(serviceList.hours,serviceList.status,serviceList.idOrder,serviceList.staff.name,serviceList.service.name);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "serviceLists.xlsx");


                }
            }

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
