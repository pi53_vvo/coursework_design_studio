﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using CourseWork.Models;
using Newtonsoft.Json;

namespace CourseWork.Controllers
{
    public class servicesController : Controller
    {
        private designStudioEntities db = new designStudioEntities();

        // GET: services
        public ActionResult Index(DateTime? from, DateTime? to, string sortOrder, string SearchString_name, string SearchString_cost_1, string SearchString_cost_2)
        {
            if (!from.HasValue || !to.HasValue)
            {
                from = new DateTime(2018, 10, 1);
                to = new DateTime(2018, 12, 31);
            }
            var res = db.MostOrderedServices(from, to).ToList();
            ViewBag.pieChartData = res;
            ViewBag.from = from.ToString().Substring(0, 10);
            ViewBag.to = to.ToString().Substring(0, 10);

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.pricePerHourSortParm = sortOrder == "pricePerHour" ? "pricePerHour_desc" : "pricePerHour";


            var services = from s in db.services
                           select s;
            switch (sortOrder)
            {
                case "name_desc":
                    services = services.OrderByDescending(s => s.name);
                    break;
                case "pricePerHour":
                    services = services.OrderBy(s => s.pricePerHour);
                    break;
                case "pricePerHour_desc":
                    services = services.OrderByDescending(s => s.pricePerHour);
                    break;


                default:
                    services = services.OrderBy(s => s.name);
                    break;
            }

            if (!String.IsNullOrEmpty(SearchString_name) || !String.IsNullOrEmpty(SearchString_cost_1) || !String.IsNullOrEmpty(SearchString_cost_2))
            {
                if (!String.IsNullOrEmpty(SearchString_cost_1) || !String.IsNullOrEmpty(SearchString_cost_2))
                {
                    int cost_1 = Convert.ToInt32(SearchString_cost_1);
                    int cost_2 = Convert.ToInt32(SearchString_cost_2);

                    services = services.Where(s => s.name.Contains(SearchString_name) && (s.pricePerHour >= cost_1 && s.pricePerHour <= cost_2)
                                      );
                }

                services = services.Where(s => s.name.Contains(SearchString_name)
                                       );
            }

            string preserveReferenacesObjects = JsonConvert.SerializeObject(services, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });

            ViewBag.data = preserveReferenacesObjects;

            return View(services.ToList());

        }

        // GET: services/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            service service = db.services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // GET: services/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: services/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,pricePerHour,description")] service service)
        {
            if (ModelState.IsValid)
            {
                db.services.Add(service);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(service);
        }

        // GET: services/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            service service = db.services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: services/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,pricePerHour,description")] service service)
        {
            if (ModelState.IsValid)
            {
                db.Entry(service).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(service);
        }

        // GET: services/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            service service = db.services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service service = db.services.Find(id);
            db.services.Remove(service);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public FileResult Export(string data)
        {


            designStudioEntities db = new designStudioEntities();

            DataTable dt = new DataTable("Services");
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("Name Service"),
                                            new DataColumn("pricePerHour"),
                                            new DataColumn("Description")
                                            
                                            });





            var services = JsonConvert.DeserializeObject<List<service>>(data);

            foreach (var service in services)
            {
                dt.Rows.Add(service.name,service.pricePerHour,service.description);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Services.xlsx");


                }
            }

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
