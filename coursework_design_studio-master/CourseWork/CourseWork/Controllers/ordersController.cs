﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using CourseWork.Attributes;
using CourseWork.Models;
using Newtonsoft.Json;
using System.Web.Script.Serialization;



namespace CourseWork.Controllers
{
    [MyAuthorize(Roles = "admin, projManager, Employee, client")]
    public class ordersController : Controller
    {
        private designStudioEntities db = new designStudioEntities();
        private User currentUser;

        private User GetCurrentUser()
        {
            IEnumerable<Claim> claims = ((ClaimsIdentity)User.Identity).Claims;
            var id = int.Parse(claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier).Value);
            currentUser = db.Users.FirstOrDefault(u => u.id == id);
            return currentUser;
        }
        // GET: orders
        public ActionResult Index(string sortOrder,string date_1, string date_2, string SearchString_FIO, string SearchString_team, int? year=2018)
        {
            GetCurrentUser();

            var orders = new List<order>();
            orders = db.orders.Include(o => o.client).Include(o => o.team).ToList();
            if (User.IsInRole("projManager") || User.IsInRole("Employee"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam;
                orders = db.orders.Where(o => o.idTeam == teamId).ToList();//.Include(o => o.client).Include(o => o.team).ToList();
            }
            if (User.IsInRole("client"))
            {
                orders = db.orders.Where(o => o.client.idUser == currentUser.id).ToList();
            }
            ViewBag.data = db.OrdersPriceToHoursProcedure().ToList();
            ViewBag.data1 = db.ordersAmountPerYear(year).ToList();
            ViewBag.year = year;
            //

            ViewBag.PriceTotalSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.HoursTotalSortParm = sortOrder == "HoursTotal" ? "HoursTotal_desc" : "HoursTotal";
            ViewBag.DateStartSortParm = sortOrder == "DateStart" ? "DateStart_desc" : "DateStart";
            ViewBag.DateFinishSortParm = sortOrder == "DateFinish" ? "DateFinish_desc" : "DateFinish";
            ViewBag.ClientFioSortParm = sortOrder == "ClientFio" ? "ClientFio_desc" : "ClientFio";

           var orders_ = from o in db.orders
                          select o;

          

            switch (sortOrder)
            {
                case "name_desc":
                    orders_ = orders_.OrderByDescending(o => o.priceTotal);
                    break;
                case "ClientFio":
                    orders_ = orders_.OrderBy(o => o.client.fio);
                    break;
                case "ClientFio_desc":
                    orders_ = orders_.OrderByDescending(o => o.client.fio);
                    break;
                case "HoursTotal":
                    orders_ = orders_.OrderBy(o => o.hoursTotal);
                    break;
                case "HoursTotal_desc":
                    orders_ = orders_.OrderByDescending(o => o.hoursTotal);
                    break;
                case "DateStart":
                    orders_ = orders_.OrderBy(o => o.startDate);
                    break;
                case "DateStart_desc":
                    orders_ = orders_.OrderByDescending(o => o.startDate);
                    break;
                case "DateFinish":
                    orders_ = orders_.OrderBy(o => o.finishDate);
                    break;
                case "DateFinish_desc":
                    orders_ = orders_.OrderByDescending(o => o.finishDate);
                    break;
               

                default:
                    orders_ = orders_.OrderBy(o => o.priceTotal);
                    break;
            }

            if (!String.IsNullOrEmpty(date_1) || !String.IsNullOrEmpty(date_2) || !String.IsNullOrEmpty(SearchString_FIO) || !String.IsNullOrEmpty(SearchString_team))
            {



                orders_ = orders_.Where(u =>u.startDate.ToString().Contains(date_1) && u.finishDate.ToString().Contains(date_2) 
                                       && u.client.fio.Contains(SearchString_FIO) && u.team.name.Contains(SearchString_team)
                                       );
            }

            //ViewBag.data = JsonConvert.SerializeObject(orders_.ToList());
           // var jsonSerialiser = new JavaScriptSerializer();
          //  ViewBag.json = jsonSerialiser.SerializeObject(orders_.ToList());

            string preserveReferenacesObjects = JsonConvert.SerializeObject(orders_, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });

            ViewBag.dataY = preserveReferenacesObjects;
            var list = orders_.ToList();
            return View(list);
        }

        // GET: orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: orders/Create

        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create()
        {
            GetCurrentUser();
            ViewBag.idClient = new SelectList(db.clients, "id", "fio");
            ViewBag.idTeam = new SelectList(db.teams, "id", "name");
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idTeam = new SelectList(db.teams.Where(t => t.id == teamId), "id", "name");
            }
                
            return View();
        }

        // POST: orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create([Bind(Include = "id,idClient,idTeam,priceTotal,hoursTotal,startDate,finishDate")] order order)
        {
            if (ModelState.IsValid)
            {
                order.finishDate = DateTime.Now;
                db.orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idClient = new SelectList(db.clients, "id", "fio", order.idClient);
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", order.idTeam);
            return View(order);
        }

        // GET: orders/Edit/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Edit(int? id)
        {
            GetCurrentUser();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.idClient = new SelectList(db.clients, "id", "fio", order.idClient);
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", order.idTeam);
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idTeam = new SelectList(db.teams.Where(t => t.id == teamId), "id", "name");
            }
            return View(order);
        }

        // POST: orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Edit([Bind(Include = "id,idClient,idTeam,priceTotal,hoursTotal,startDate,finishDate")] order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idClient = new SelectList(db.clients, "id", "fio", order.idClient);
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", order.idTeam);
            return View(order);
        }

        // GET: orders/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: orders/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            order order = db.orders.Find(id);
            db.orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public FileResult Export(string data)
        {


            designStudioEntities db = new designStudioEntities();

            DataTable dt = new DataTable("Orders");
            dt.Columns.AddRange(new DataColumn[6] { new DataColumn("PriceTotal"),
                                            new DataColumn("HoursTotal"),
                                            new DataColumn("DateStart"),
                                            new DataColumn("DateFinish"),
                                            new DataColumn("ClientFio"),
                                            new DataColumn("Team Name"),
                                            });



            

            var orders = JsonConvert.DeserializeObject<List <order>>(data);

            foreach (var order in orders)
            {
                dt.Rows.Add(order.priceTotal,order.hoursTotal,order.startDate,order.finishDate,order.client.fio,order.team.name);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Orders.xlsx");


                }
            }

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
