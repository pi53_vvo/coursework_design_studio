﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using CourseWork.Models;

namespace CourseWork.Controllers
{
    [Authorize(Roles = "admin, projManager")]
    public class staffsController : Controller
    {
        private designStudioEntities db = new designStudioEntities();
        private User currentUser;

        private User GetCurrentUser()
        {
            IEnumerable<Claim> claims = ((ClaimsIdentity)User.Identity).Claims;
            var id = int.Parse(claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier).Value);
            currentUser = db.Users.FirstOrDefault(u => u.id == id);
            return currentUser;
        }
        // GET: staffs
        public ActionResult Index(string sortOrder,string SearchString_FIO, string SearchString_position, string SearchString_team, int? year=2018)
        {
            GetCurrentUser();
            var staffs = new List<staff>();
            staffs = db.staffs.Include(s => s.team).Include(s => s.User).ToList();
            ViewBag.data = db.hoursWorkedByAllTeams(year);
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam;
                staffs = db.staffs.Where(s => s.idTeam == teamId).Include(s => s.team).Include(s => s.User).ToList();
                ViewBag.data = db.hoursWorkedAtTeam(year, teamId);
            }
            ViewBag.year = year;

            ViewBag.WorkerNameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateStartSortParm = sortOrder == "DateStart" ? "DateStart_desc" : "DateStart";
         

            var staffs_ = from s in db.staffs
                                select s;



            switch (sortOrder)
            {
                case "name_desc":
                    staffs_ = staffs_.OrderByDescending(s => s.name);
                    break;

                case "DateStart":
                    staffs_ = staffs_.OrderBy(s => s.enterDate);
                    break;
                case "DateStart_desc":
                    staffs_ = staffs_.OrderByDescending(s => s.enterDate);
                    break;
               




                default:
                    staffs_ = staffs_.OrderBy(s => s.name);
                    break;
            }


            //string SearchString_FIO, string SearchString_position, string SearchString_team

            if (!String.IsNullOrEmpty(SearchString_FIO) || !String.IsNullOrEmpty(SearchString_position) || !String.IsNullOrEmpty(SearchString_team))
            {



                staffs_ = staffs_.Where(s => s.name.Contains(SearchString_FIO) && s.position.Contains(SearchString_position) && s.team.name.Contains(SearchString_team)
                                       );
            }


            return View(staffs_);
        }

        // GET: staffs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            staff staff = db.staffs.Find(id);
            if (staff == null)
            {
                return HttpNotFound();
            }
            return View(staff);
        }

        // GET: staffs/Create
        public ActionResult Create()
        {
            ViewBag.idTeam = new SelectList(db.teams, "id", "name");
            ViewBag.idUser = new SelectList(db.Users, "id", "login");
            return View();
        }

        // POST: staffs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,idTeam,name,enterDate,position,idUser")] staff staff)
        {
            if (ModelState.IsValid)
            {
                db.staffs.Add(staff);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idTeam = new SelectList(db.teams, "id", "name", staff.idTeam);
            ViewBag.idUser = new SelectList(db.Users, "id", "login", staff.idUser);
            return View(staff);
        }

        // GET: staffs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            staff staff = db.staffs.Find(id);
            if (staff == null)
            {
                return HttpNotFound();
            }
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", staff.idTeam);
            ViewBag.idUser = new SelectList(db.Users, "id", "login", staff.idUser);
            return View(staff);
        }

        // POST: staffs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,idTeam,name,enterDate,position,idUser")] staff staff)
        {
            if (ModelState.IsValid)
            {
                db.Entry(staff).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", staff.idTeam);
            ViewBag.idUser = new SelectList(db.Users, "id", "login", staff.idUser);
            return View(staff);
        }

        // GET: staffs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            staff staff = db.staffs.Find(id);
            if (staff == null)
            {
                return HttpNotFound();
            }
            return View(staff);
        }

        // POST: staffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            staff staff = db.staffs.Find(id);
            db.staffs.Remove(staff);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
