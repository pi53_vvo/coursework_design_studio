﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using CourseWork.Attributes;
using CourseWork.Models;

namespace CourseWork.Controllers
{
    [MyAuthorize(Roles = "admin, projManager, Employee")]
    public class ClientsController : Controller
    {
        private designStudioEntities db = new designStudioEntities();
        private User currentUser;

        private User GetCurrentUser()
        {
            IEnumerable<Claim> claims = ((ClaimsIdentity)User.Identity).Claims;
            var id = int.Parse(claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier).Value);
            currentUser = db.Users.FirstOrDefault(u => u.id == id);
            return currentUser;
        }

        // GET: Clients
        public ActionResult Index(string sortOrder, string SearchString_FIO, string SearchString_number, string SearchString_address)
        {
            ViewBag.FioSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.RegDateSortParm = sortOrder == "RegDate" ? "RegDate_desc" : "RegDate";


            var clients = from s in db.clients
                          select s;
            switch (sortOrder)
            {
                case "name_desc":
                    clients = clients.OrderByDescending(s => s.fio);
                    break;
                case "RegDate":
                    clients = clients.OrderBy(s => s.regDate);
                    break;
                case "RegDate_desc":
                    clients = clients.OrderByDescending(s => s.regDate);
                    break;


                default:
                    clients = clients.OrderBy(s => s.fio);
                    break;

                    
            }

            

            if (!String.IsNullOrEmpty(SearchString_FIO) || !String.IsNullOrEmpty(SearchString_number) || !String.IsNullOrEmpty(SearchString_address))
            {


                clients = clients.Where(s => s.fio.Contains(SearchString_FIO) && s.phoneNumber.Contains(SearchString_number) && s.address.Contains(SearchString_address)
                                       );
            }

            

            return View(clients);
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // GET: Clients/Create

        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create()
        {
            ViewBag.idUser = new SelectList(db.Users.Where(u => u.role == "client"), "id", "login");
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create([Bind(Include = "id,fio,phoneNumber,address,regDate,idUser")] client client)
        {
            if (ModelState.IsValid)
            {
                db.clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(client);
        }

        // GET: Clients/Edit/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            ViewBag.idUser = new SelectList(db.Users.Where(u => u.role == "client"), "id", "login", client.idUser);
            return View(client);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Edit([Bind(Include = "id,fio,phoneNumber,address,regDate,idUser")] client client)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idUser = new SelectList(db.Users.Where(u => u.role == "client"), "id", "login", client.idUser);
            return View(client);
        }

        // GET: Clients/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: Clients/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            client client = db.clients.Find(id);
            db.clients.Remove(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public FileResult Export()
        {

            
          designStudioEntities db = new designStudioEntities();
            
          DataTable dt = new DataTable("Clients");
            dt.Columns.AddRange(new DataColumn[5] { new DataColumn("FIO"),
                                            new DataColumn("Phone Number"),
                                            new DataColumn("Address"),
                                            new DataColumn("Date Registration"),
                                            new DataColumn("Login")

                                            }  );



            var clients = from c in db.clients
                          join u in db.Users on c.idUser equals u.id
                          select new { FIO = c.fio, PN=c.phoneNumber,adr=c.address,rd=c.regDate,lg=u.login};


            foreach (var client in clients)
            {
                dt.Rows.Add(client.FIO,client.PN, client.adr, client.rd,client.lg);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Clients.xlsx");
                    
                    
                }
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
