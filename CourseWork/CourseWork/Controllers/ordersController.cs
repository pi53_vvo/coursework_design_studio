﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using CourseWork.Attributes;
using CourseWork.Models;

namespace CourseWork.Controllers
{
    [MyAuthorize(Roles = "admin, projManager, Employee, client")]
    public class ordersController : Controller
    {
        private designStudioEntities db = new designStudioEntities();
        private User currentUser;

        private User GetCurrentUser()
        {
            IEnumerable<Claim> claims = ((ClaimsIdentity)User.Identity).Claims;
            var id = int.Parse(claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier).Value);
            currentUser = db.Users.FirstOrDefault(u => u.id == id);
            return currentUser;
        }
        // GET: orders
        public ActionResult Index(int? year=2018)
        {
            GetCurrentUser();

            var orders = new List<order>();
            orders = db.orders.Include(o => o.client).Include(o => o.team).ToList();
            if (User.IsInRole("projManager") || User.IsInRole("Employee"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam;
                orders = db.orders.Where(o => o.idTeam == teamId).ToList();//.Include(o => o.client).Include(o => o.team).ToList();
            }
            if (User.IsInRole("client"))
            {
                orders = db.orders.Where(o => o.client.idUser == currentUser.id).ToList();
            }
            ViewBag.data = db.OrdersPriceToHoursProcedure().ToList();
            ViewBag.data1 = db.ordersAmountPerYear(year).ToList();
            ViewBag.year = year;
            //
            return View(orders);
        }

        // GET: orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: orders/Create

        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create()
        {
            GetCurrentUser();
            ViewBag.idClient = new SelectList(db.clients, "id", "fio");
            ViewBag.idTeam = new SelectList(db.teams, "id", "name");
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idTeam = new SelectList(db.teams.Where(t => t.id == teamId), "id", "name");
            }
                
            return View();
        }

        // POST: orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create([Bind(Include = "id,idClient,idTeam,priceTotal,hoursTotal,startDate,finishDate")] order order)
        {
            if (ModelState.IsValid)
            {
                db.orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idClient = new SelectList(db.clients, "id", "fio", order.idClient);
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", order.idTeam);
            return View(order);
        }

        // GET: orders/Edit/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Edit(int? id)
        {
            GetCurrentUser();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.idClient = new SelectList(db.clients, "id", "fio", order.idClient);
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", order.idTeam);
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idTeam = new SelectList(db.teams.Where(t => t.id == teamId), "id", "name");
            }
            return View(order);
        }

        // POST: orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Edit([Bind(Include = "id,idClient,idTeam,priceTotal,hoursTotal,startDate,finishDate")] order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idClient = new SelectList(db.clients, "id", "fio", order.idClient);
            ViewBag.idTeam = new SelectList(db.teams, "id", "name", order.idTeam);
            return View(order);
        }

        // GET: orders/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: orders/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            order order = db.orders.Find(id);
            db.orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
