﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using CourseWork.Attributes;
using CourseWork.Models;

namespace CourseWork.Controllers
{
    [MyAuthorize(Roles = "admin, projManager, Employee, client")]
    public class serviceListsController : Controller
    {
        private designStudioEntities db = new designStudioEntities();
        private User currentUser;

        private User GetCurrentUser()
        {
            IEnumerable<Claim> claims = ((ClaimsIdentity)User.Identity).Claims;
            var id = int.Parse(claims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier).Value);
            currentUser = db.Users.FirstOrDefault(u => u.id == id);
            return currentUser;
        }
        // GET: serviceLists

        public ActionResult Index(int? id)
        {
            var serviceLists = new List<serviceList>();
            GetCurrentUser();
            if (User.IsInRole("client"))
            {
                serviceLists = db.serviceLists.Where(s => s.order.id == id).Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();

                return View(serviceLists);
            }
            serviceLists = db.serviceLists.Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam;
                serviceLists = db.serviceLists.Where(s => s.order.idTeam == teamId).Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();
            }
            if (User.IsInRole("Employee"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam;
                serviceLists = db.serviceLists.Where(s => s.order.idTeam == teamId && s.staff.idUser == currentUser.id).Include(s => s.order).Include(s => s.staff).Include(s => s.service).ToList();
            }
            return View(serviceLists);
        }

        // GET: serviceLists/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceList serviceList = db.serviceLists.Find(id);
            if (serviceList == null)
            {
                return HttpNotFound();
            }
            return View(serviceList);
        }

        // GET: serviceLists/Create

        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create()
        {
            ViewBag.idOrder = new SelectList(db.orders, "id", "id");
            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", "team.name", 1);
            ViewBag.idService = new SelectList(db.services, "id", "name");

            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }

            return View();
        }

        // POST: serviceLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Create([Bind(Include = "id,idService,idOrder,idEmployee,hours,status")] serviceList serviceList)
        {
            if (ModelState.IsValid)
            {
                db.serviceLists.Add(serviceList);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idOrder = new SelectList(db.orders, "id", "id", serviceList.idOrder);
            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", serviceList.idEmployee);
            ViewBag.idService = new SelectList(db.services, "id", "name", serviceList.idService);
            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }
            return View(serviceList);
        }

        // GET: serviceLists/Edit/5
        [MyAuthorize(Roles = "admin, projManager, Employee")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceList serviceList = db.serviceLists.Find(id);
            if (serviceList == null)
            {
                return HttpNotFound();
            }
            ViewBag.idOrder = new SelectList(db.orders, "id", "id", serviceList.idOrder);

            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", "team.name", serviceList.idEmployee);

            ViewBag.idService = new SelectList(db.services, "id", "name", serviceList.idService);
            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }
            return View(serviceList);
        }

        // POST: serviceLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MyAuthorize(Roles = "admin, projManager, Employee")]
        public ActionResult Edit([Bind(Include = "id,idService,idOrder,idEmployee,hours,status")] serviceList serviceList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idOrder = new SelectList(db.orders, "id", "id", serviceList.idOrder);
            ViewBag.idEmployee = new SelectList(db.staffs, "id", "name", serviceList.idEmployee);
            ViewBag.idService = new SelectList(db.services, "id", "name", serviceList.idService);
            GetCurrentUser();
            if (User.IsInRole("projManager"))
            {
                var teamId = db.staffs.Where(s => s.idUser == currentUser.id).ToList()[0].idTeam.Value;
                ViewBag.idOrder = new SelectList(db.orders.Where(o => o.idTeam == teamId), "id", "id");
                ViewBag.idEmployee = new SelectList(db.staffs.Where(s => s.idTeam == teamId), "id", "name", "team.name", 1);
            }
            return View(serviceList);
        }

        // GET: serviceLists/Delete/5
        [MyAuthorize(Roles = "admin, projManager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceList serviceList = db.serviceLists.Find(id);
            if (serviceList == null)
            {
                return HttpNotFound();
            }
            return View(serviceList);
        }

        // POST: serviceLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [MyAuthorize(Roles = "admin, projManager")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            serviceList serviceList = db.serviceLists.Find(id);
            db.serviceLists.Remove(serviceList);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
